**Useful Command Prompt (CMD) Tricks You Should Know**

Today in this post we will tell you maybe minor but curious tricks and commands for the Windows CMD that apply from the version of Windows 98 to the new Windows 10.

But, wait, as now many of you might be thinking that what is CMD? 

The CMD is the shell that runs on the Windows operating system. It is also known as Command prompt and works by using an application called Cmd.exe located on the hard disk that you have installed the operating system and then entering a /Windows/System32. The CMD is used to perform functions in the operating system such as open programs, modify and view files and folders or change system settings.

Let me explain in brief, the CMD (Command Prompt) is also the command interpreter of the CM-2 or DOS system that until our time comes included in the systems of Microsoft Windows, not as an operating system but as an application, from this application we can handle our system, in the form of commands and not graphs. 

Also, many graphical applications redirect parts of their functions to the CMD to be emulated in the same to execute them in the system. In other words, it is a useful application from which we could save our pc and even recover information. 

However, to open the CMD just open the "Run" by taping Windows button along with the "R" key on your keyboard and then simply type cmd and then simply press the "ENTER". You can also open it by searching it in the start menu or in the folder that is located which I already specified in the previous paragraph.

Here, are some common commands used in CMD

The number of commands that exist in the CMD is many, so I will focus on the ones I use the most.

IPCONFIG/ALL: This displays the IP configuration information of your computer. Showing your local IP address, default gateway, MAC Address, DNS, among other related information.

IPCONFIG/FLUSHDNS: This command is used to clear the cache of DNS servers. Resolved domains are cached so that they do not have to be resolved again. Cleansing the cache is resolved again.

TRACERT: It is a command used to see the path taken by your computer to another network location. It is widely used to diagnose and detect connection loss problems.

PING: Ping is used to test connections to a specific network destination, with ping you can know the response time between your computer and a server.

START: This command used to start programs and processes in Windows.

NSLOOKUP: It displays information about the DNS servers assigned to your computer.

So, all these are just some commands because as I have explained there are many. You can also open programs or navigate to routes only using commands.

Everything we have mentioned about the "cmd" could be considered as arbitrary, basic or simply as a specialized function for those who have a full knowledge of its correct use, there are some more functions that surely we will need at any time and that right now, we'll suggest you as few [cmd tricks](https://techcaption.com/useful-command-prompt-cmd-tricks-know/) to use when you need those features.

Here, are they:

doskey /history: With this command, you can easily track all your command history. Yes, it will simply let you track down all your command history.

sfc /scannow: If you are facing some errors on your computer, then simply run this command, as it will be very helpful. If any files are missing or corrupted on your compauter, then simply this command will fix them.

These are some of the useful CMD commands that are of great use. Which command do you use the most? Join the discussion with us.
